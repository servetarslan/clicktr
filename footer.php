<!-- footer begin -->
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-12">
                        <img src="<?=HTTP_RESIM.strip_tags($ceks["resim"]);?>" class="logo-small" alt=""><br>
                        <?=strip_tags(trim($ceks["fouter"])); ?>
                    </div>

                    <div class="col-md-4 col-sm-6">
                        <div class="widget widget_recent_post">
                            <ul>
                                <?php 
                                    $menurow = mysql_query("select * from kategori where tur = 'Menu' and menu != 1 and dil = 'tr' and durum = '1' order by sira asc");
                                    while($menu = mysql_fetch_array($menurow)){
                                ?>
                                <li><a href="<?=HTTP_SITE.strip_tags($menu['link']);?>"><?=strip_tags($menu['ad']);?></a></li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>

                    <div class="col-md-4 col-sm-6">
                        <div class="widget widget-address">
                            <address>
                                <span><strong>Adres:</strong><?=strip_tags(trim($ceks["address"])); ?></span>
                                <span><strong>Telefon:</strong><?=strip_tags(trim($ceks["tel"])); ?></span>
                                
                                <span><strong>Email:</strong><a href="mailto:<?=strip_tags(trim($ceks["mail"])); ?>"><?=strip_tags(trim($ceks["mail"])); ?></a></span>
                            </address>
                        </div>
                    </div>
                </div>
            </div>

            <div class="subfooter">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                                               
                        </div>
                        <div class="col-md-6 text-right mobile-center">
                            <div class="social-icons">
                                <a href="<?=strip_tags(trim($ceks["facebook"])); ?>" target="_blank"><i class="fab fa-facebook fa-lg"></i></a>
                                <a href="<?=strip_tags(trim($ceks["linkedin"])); ?>" target="_blank"><i class="fab fa-linkedin fa-lg"></i></a>
                                
                                <a href="<?=strip_tags(trim($ceks["twitter"])); ?>" target="_blank"><i class="fab fa-instagram fa-lg"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a href="#" id="back-to-top"></a>
        </footer>