<?
include("../inc/config.php");
include("../inc/fonk.php");
$ceks = mysql_fetch_array(mysql_query("select * from ayar where dil = 'en'"));

$pages = 'iletisim';

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>İletişim <?=$ceks["title"]?></title>
	<meta name="robots" content="index, follow" >
    <meta name="description" content="<?=$ceks["description"]?>" />    
    <meta name="author" content="">


    <!--[if lt IE 9]>
	<script src="<?=HTTP_SITE; ?>assets/js/html5shiv.js"></script>
	<![endif]-->


    <!-- CSS Files
    ================================================== -->
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/css/jpreloader.css" type="text/css">
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/css/animate.css" type="text/css">
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/css/plugin.css" type="text/css">
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/css/owl.carousel.css" type="text/css">
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/css/owl.theme.css" type="text/css">
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/css/owl.transitions.css" type="text/css">
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/css/magnific-popup.css" type="text/css">
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/css/style.css" type="text/css">
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/demo/demo.css" type="text/css">

    <!-- custom background -->
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/css/bg.css" type="text/css">

    <!-- color scheme -->
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/css/color.css" type="text/css" id="colors">

    <!-- load fonts -->
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/fonts/font-awesome/css/font-awesome.css" type="text/css">
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/fonts/elegant_font/HTML_CSS/style.css" type="text/css">
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/fonts/fontawesome/css/all.css" type="text/css">
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/fonts/et-line-font/style.css" type="text/css">

</head>
<body class="page-about">

    <div id="wrapper">

       <?php include('header.php'); ?>
    
            <!-- content begin -->
            <div id="content" class="no-top">
                <section class="no-top bg-wt" aria-label="map-container">
                    <?=$ceks["maps"]?>
                </section>
    
                <div class="container">
                    <div class="row">
    
                        <div class="col-md-12">
                            <h3>Mesaj Gönderin</h3>
                            <form name="contactForm" id='contact_form' method="post" action='#'>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div id='name_error' class='error'>Please enter your name.</div>
                                        <div>
                                            <input type='text' name='name' id='name' class="form-control" placeholder="Ad Soyad">
                                        </div>
    
                                        <div id='email_error' class='error'>Please enter your valid E-mail ID.</div>
                                        <div>
                                            <input type='text' name='email' id='email' class="form-control" placeholder="Eposta">
                                        </div>
    
                                        <div id='phone_error' class='error'>Please enter your phone number.</div>
                                        <div>
                                            <input type='text' name='phone' id='phone' class="form-control" placeholder="Telefon">
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div id='message_error' class='error'>Please enter your message.</div>
                                        <div>
                                            <textarea name='message' id='message' class="form-control" placeholder="Mesajınız"></textarea>
                                        </div>
                                    </div>
    
                                    <div class="col-md-12">
                                        <p id='submit'>
                                            <input type='submit' id='send_message' value='Gönder' class="btn btn-line">
                                        </p>
                                    </div>
                                </div>
                            </form>
                        </div>
    
               
                    </div>
                </div>
            </div>

            <?php include('footer.php'); ?>
    </div>
    <!-- style switcher
    ================================================== -->
    


    <!-- Javascript Files
    ================================================== -->
    <script src="<?=HTTP_SITE; ?>assets/js/jquery.min.js"></script>
    <script src="<?=HTTP_SITE; ?>assets/js/jpreLoader.js"></script>
    <script src="<?=HTTP_SITE; ?>assets/js/bootstrap.min.js"></script>
    <script src="<?=HTTP_SITE; ?>assets/js/jquery.isotope.min.js"></script>
    <script src="<?=HTTP_SITE; ?>assets/js/easing.js"></script>
    <script src="<?=HTTP_SITE; ?>assets/js/jquery.flexslider-min.js"></script>
    <script src="<?=HTTP_SITE; ?>assets/js/jquery.scrollto.js"></script>
    <script src="<?=HTTP_SITE; ?>assets/js/owl.carousel.js"></script>
    <script src="<?=HTTP_SITE; ?>assets/js/jquery.countTo.js"></script>
    <script src="<?=HTTP_SITE; ?>assets/js/classie.js"></script>
    <script src="<?=HTTP_SITE; ?>assets/js/video.resize.js"></script>
    <script src="<?=HTTP_SITE; ?>assets/js/validation.js"></script>
    <script src="<?=HTTP_SITE; ?>assets/js/wow.min.js"></script>
    <script src="<?=HTTP_SITE; ?>assets/js/jquery.magnific-popup.min.js"></script>
    <script src="<?=HTTP_SITE; ?>assets/js/enquire.min.js"></script>
    <script src="<?=HTTP_SITE; ?>assets/js/designesia.js"></script>
    <script src="<?=HTTP_SITE; ?>assets/demo/demo.js"></script>

</body>
</html>
