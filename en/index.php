<?
include("../inc/config.php");
include("../inc/fonk.php");
$ceks = mysql_fetch_array(mysql_query("select * from ayar where dil = 'en'"));

$sliderrow = mysql_query("select * from kategori where tur = 'Slider' and dil = 'en' and durum = '1' order by sira asc");
$clientrow = mysql_query("select * from kategori where tur = 'Client' and dil = 'en' and durum = '1' order by sira asc");
$pagerow = mysql_fetch_array(mysql_query("select * from kategori where tur = 'Home' and dil = 'en' and durum = '1' order by id desc"));


$pages = 'index';

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?=$ceks["title"]?></title>
	<meta name="robots" content="index, follow" >
    <meta name="description" content="<?=$ceks["description"]?>" />    
    <meta name="author" content="">


    <!--[if lt IE 9]>
	<script src="<?=HTTP_SITE; ?>assets/js/html5shiv.js"></script>
	<![endif]-->


    <!-- CSS Files
    ================================================== -->
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/css/jpreloader.css" type="text/css">
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/css/animate.css" type="text/css">
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/css/plugin.css" type="text/css">
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/css/owl.carousel.css" type="text/css">
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/css/owl.theme.css" type="text/css">
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/css/owl.transitions.css" type="text/css">
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/css/magnific-popup.css" type="text/css">
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/css/style.css" type="text/css">
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/demo/demo.css" type="text/css">

    <!-- custom background -->
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/css/bg.css" type="text/css">

    <!-- color scheme -->
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/css/color.css" type="text/css" id="colors">

    <!-- load fonts -->
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/fonts/font-awesome/css/font-awesome.css" type="text/css">
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/fonts/elegant_font/HTML_CSS/style.css" type="text/css">
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/fonts/fontawesome/css/all.css" type="text/css">
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/fonts/et-line-font/style.css" type="text/css">

</head>

<body id="homepage">

    <div id="wrapper">

        <?php include('header.php'); ?>

        <!-- content begin -->
        <div id="content" class="no-bottom no-top">

            <!-- owl slider begin -->
            <section class="home-slider">
                <div class="slider-main owl-carousel owl-theme" id="homeSlider">
                <?php 
                    while($slider = mysql_fetch_array($sliderrow)){
                ?>
                    <!-- slider item start -->
                    <div class="slider-item" style="background: url('<?php echo HTTP_RESIM.strip_tags($slider['resim']);?>')">
                        <div class="container">
                            <div class="slider-text">
                                <h3 class="slider-head"><?php echo strip_tags($slider['ad']);?></h3>
                                <p class="slidet-pragraph"><?php echo strip_tags($slider['ozet']);?></p>
                                <a href="<?php echo strip_tags($slider['link']);?>" class="btn btn-line-turkish btn-big slider-btn"><?php echo strip_tags($slider['artdirector']);?></a>
                            </div>
                        </div>
                    </div>
                    <!-- slider item end  -->
                    <?php } ?>

                </div>
            </section>
            <!-- owl slider close -->

            <!-- <div id="section-describe" class="side-bg text-light">
                
                <div class="container">
                        
                        <div class="col-md-6 col-sm-6 pull-left" data-delay="0">
                            <img src="<?=HTTP_SITE; ?>assets/images/bg/Anasayfa_app_gOrseli.png" alt="" class="img-responsive slideInLeft">
                        </div>
                
                        <div class="col-md-6 wow fadeInRight" data-wow-delay=".25s">
                            <h2 class="services-head">Influencer’ım</h2>

                            <p class="text-dark">Markaların kampanyalarını ve ürünlerini paylaşarak kazanmak istiyorum.</p>    
                            <a href="influencer.html" class="btn btn-line-turkish btn-big scroll-to">Hemen Tıkla</a>
                            <br>
                            <br>
                            <h2 class="services-head">Markayım veya Ajansım</h2>
                            <p class="text-dark">Alanıma uygun influencer’lar, ürünlerimi veya kampanyalarımı paylaşsın istiyorum.</p>    
                            <a href="firma.html" class="btn btn-line-turkish btn-big scroll-to">Hemen Tıkla</a>
                        </div>

                </div>
                    </div>

            <div id="section-services" class="bg-wt" >
                <div class="container" >
                    <div class="row" >
                        <div class="col-md-6 col-md-offset-3 text-center wow fadeInUp animated" style="visibility: visible; animation-name: fadeInUp; background-size: cover;">
                            <h1>Kazandıran içerik ağını keşfedin!</h1>
                            <div class="separator" ><span><i class="far fa-circle"></i></span></div>
                        </div>


                        <div class="col-md-4 col-sm-6 wow fadeInLeft animated pad-10" style="visibility: visible; animation-name: fadeInLeft; background-size: cover;">
                            <div class="box-icon-simple right" >
                                <i class="icon-wallet wow bounceIn animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: bounceIn;"></i>
                                <div class="text" >
                                    Her alandan yüzlerce influencer ve markanın buluşması noktası; Turkish Clicks!                                </div>
                            </div>
                            <div class="spacer-double" ></div>
                            <div class="box-icon-simple right" >
                                <i class="icon-pencil wow bounceIn animated" data-wow-delay=".75s" style="visibility: visible; animation-delay: 0.75s; animation-name: bounceIn;"></i>
                                <div class="text" >
                                    Markaların, ürün, kampanya veya duyurularını paylaşarak sosyal medya hesabınız üzerinden kazanç elde edin.                                </div>
                            </div>
                            <div class="spacer-double" ></div>
                            <div class="box-icon-simple right last-right" >
                                <i class="icon-pictures wow bounceIn animated" data-wow-delay="1s" style="visibility: visible; animation-delay: 1s; animation-name: bounceIn;"></i>
                                <div class="text" >
                                    Markanızın hizmet verdiği sektöre özel influencer’lar, ürün, kampanya veya duyurularınızı paylaşsın, milyonlarca insana ulaşın.                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 wow fadeInUp animated tcenter ipad-hidden"  data-wow-delay=".2s" data-wow-duration="1s" style="visibility: visible; animation-duration: 1s; animation-delay: 0.2s; animation-name: fadeInUp; background-size: cover;">
                            <img src="<?=HTTP_SITE; ?>assets/images/misc/deco_1.png" class="img-responsive" alt="" style="max-width: 300px;">
                        </div>

                        <div class="col-md-4 col-sm-6 wow fadeInRight animated pad-10" style="visibility: visible; animation-name: fadeInRight; background-size: cover;">
                            <div class="box-icon-simple left" >
                                <i class="icon-chat wow bounceIn animated" data-wow-delay=".6.5s" style="visibility: visible; animation-name: bounceIn;"></i>
                                <div class="text" >
                                    Turkish Clicks’le, sadece Türkiye’den değil, uluslararası birçok influencer veya marka ile çalışın.                                </div>
                            </div>
                            <div class="spacer-double" ></div>
                            <div class="box-icon-simple left" >
                                <i class="icon-clock wow bounceIn animated" data-wow-delay=".9s" style="visibility: visible; animation-delay: 0.9s; animation-name: bounceIn;"></i>
                                <div class="text" >
                                    Güvenilir altyapısı ve size özel tasarlanmış arayüzden kampanyanızın performansını anlık olarak kolayca takip edin.                                </div>
                            </div>
                            <div class="spacer-double" ></div>
                            <div class="box-icon-simple left" >
                                <i class="icon-upload wow bounceIn animated" data-wow-delay="1.15s" style="visibility: visible; animation-delay: 1.15s; animation-name: bounceIn;"></i>
                                <div class="text" >
                                    7 gün 24 saat hizmet veren destek ekibimiz, soru ve sorunlarınıza anında çözümler üretsin.                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div> -->

            <?=(trim($pagerow["aciklama"])); ?>


            <!-- section begin -->
            <section id="section-download" class="text-light">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3 text-center wow fadeInUp">
                    <h2 class="text-title-18"><?=(trim($pagerow["ozet"])); ?>
                            </h2>
                        <div class="separator"><span><i class="far fa-circle"></i></span></div>
                        <div class="spacer-single"></div>
                    </div>

                    <div class="col-md-3 col-sm-6 col-md-offset-3 text-center wow zoomIn">
                        <i class="fab fa-apple large id-color appicon"></i>
                        <div class="spacer-single"></div>
                        <a href="<?=strip_tags(trim($ceks["google"])); ?>" class="btn btn-line-turkish btn-big scroll-to">App Store</a>
                    </div>

                    <div class="col-md-3 col-sm-6 text-center wow zoomIn">
                        <i class="fab fa-android large id-color appicon"></i>
                        <div class="spacer-single"></div>
                        <a href="<?=strip_tags(trim($ceks["youtube"])); ?>" class="btn btn-line-turkish btn-big scroll-to">Play Store</a>
                    </div>
                </div>

                <div class="spacer-double"></div>



            </div>
            </section>
        <!-- section close -->

            <!-- section begin -->
            <section id="section-describe" class="text-light">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3 text-center wow fadeInUp">
                            <h2 class="text-title-18">References
                                </h2>
                            <div class="separator"><span><i class="far fa-circle"></i></span></div>
                            <div class="spacer-single"></div>
                        </div>

                        <div class="col-md-12">
                            <ul class="brandshome owl-carousel">
                                
                                <?php 
                                    while($slider = mysql_fetch_array($clientrow)){
                                ?>
                                    <!-- client item start -->
                                    <li><img src="<?php echo HTTP_RESIM.strip_tags($slider['resim']);?>"></li>
                                    <!-- client item end  -->

                                <?php } ?>
                            </ul>
                        </div>
                    </div>

                </div>
            </section>
        <!-- section close -->

        <?php include('footer.php'); ?>
			
			</div>


        <!-- footer close -->
        </div>
    </div>



    <!-- Javascript Files
    ================================================== -->
    <script src="<?=HTTP_SITE; ?>assets/js/jquery.min.js"></script>
    <script src="<?=HTTP_SITE; ?>assets/js/jpreLoader.js"></script>
    <script src="<?=HTTP_SITE; ?>assets/js/bootstrap.min.js"></script>
    <script src="<?=HTTP_SITE; ?>assets/js/easing.js"></script>
    <script src="<?=HTTP_SITE; ?>assets/js/jquery.scrollto.js"></script>
    <script src="<?=HTTP_SITE; ?>assets/js/owl.carousel.js"></script>
    <script src="<?=HTTP_SITE; ?>assets/js/jquery.countTo.js"></script>
    <script src="<?=HTTP_SITE; ?>assets/js/classie.js"></script>
    <script src="<?=HTTP_SITE; ?>assets/js/wow.min.js"></script>
    <script src="<?=HTTP_SITE; ?>assets/js/enquire.min.js"></script>
    <script src="<?=HTTP_SITE; ?>assets/js/designesia.js"></script>
    <script src="<?=HTTP_SITE; ?>assets/demo/demo.js"></script>

</body>
</html>
