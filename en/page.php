<?
include("../inc/config.php");
include("../inc/fonk.php");
$ceks = mysql_fetch_array(mysql_query("select * from ayar where dil = 'en'"));
$link = strip_tags(trim(editle($_GET["link"])));
$cek = mysql_fetch_array(mysql_query("select * from kategori where link = '$link' and tur != 'Menu' and durum = '1' and dil = 'en'"));
$id	 = $cek["id"];
$tur = $cek["tur"];


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?=strip_tags($cek["title"]);?></title>
    <meta name="robots" content="index, follow" >
    <meta name="description" content="<?=strip_tags($cek["ozet"]);?>" />    
    <meta name="author" content="">

    <!--[if lt IE 9]>
	<script src="<?=HTTP_SITE; ?>assets/js/html5shiv.js"></script>
	<![endif]-->


    <!-- CSS Files
    ================================================== -->
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/css/jpreloader.css" type="text/css">
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/css/animate.css" type="text/css">
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/css/plugin.css" type="text/css">
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/css/owl.carousel.css" type="text/css">
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/css/owl.theme.css" type="text/css">
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/css/owl.transitions.css" type="text/css">
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/css/magnific-popup.css" type="text/css">
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/css/style.css" type="text/css">
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/demo/demo.css" type="text/css">

    <!-- custom background -->
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/css/bg.css" type="text/css">

    <!-- color scheme -->
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/css/color.css" type="text/css" id="colors">

    <!-- load fonts -->
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/fonts/font-awesome/css/font-awesome.css" type="text/css">
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/fonts/elegant_font/HTML_CSS/style.css" type="text/css">
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/fonts/et-line-font/style.css" type="text/css">
    <link rel="stylesheet" href="<?=HTTP_SITE; ?>assets/fonts/fontawesome/css/all.css" type="text/css">

</head>
<body class="page-about">

    <div id="wrapper">

    <?php include('header.php'); ?>



    <?php if($tur == "Page"){ $sss = "no-bottom";}else{$sss="";}?>

    <!-- content begin -->
    <div id="content" class="no-top <?php echo $sss; ?>">


        <?php if($tur == "Page"){?>
            <section id="section-side-1-index-landing-product" class="side-bg no-padding half-bg" data-pic="<?php echo HTTP_RESIM.$cek['resim']; ?>">

                <div class="container" >
                    <div class="row" >
                        <div class="inner-padding" >
                            <div class="col-md-5 col-md-offset-7 wow fadeInRight animated" data-wow-delay=".5s" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInRight; ">
                                <?php echo $cek['yer']; ?>   
                            </div>
                            <div class="clearfix" ></div>
                        </div>
                    </div>
                </div>
                <? if($cek["award"] != ""){?>
                <div class="video-icon">
                    <a class="popup-youtube" href="<?=$cek["award"];?>">
                        <img src="<?=HTTP_SITE;?>assets/images/video-play-button.png">
                    </a>
                </div>
                <? } ?>
            </section>
            <?php } else{} ?>

            <?php echo $cek['aciklama']; ?>

        <?php include('footer.php'); ?>
    </div>
    <!-- style switcher
    ================================================== -->
    


    <!-- Javascript Files
    ================================================== -->
    <script src="<?=HTTP_SITE; ?>assets/js/jquery.min.js"></script>
    <script src="<?=HTTP_SITE; ?>assets/js/jpreLoader.js"></script>
    <script src="<?=HTTP_SITE; ?>assets/js/bootstrap.min.js"></script>
    <script src="<?=HTTP_SITE; ?>assets/js/jquery.isotope.min.js"></script>
    <script src="<?=HTTP_SITE; ?>assets/js/easing.js"></script>
    <script src="<?=HTTP_SITE; ?>assets/js/jquery.flexslider-min.js"></script>
    <script src="<?=HTTP_SITE; ?>assets/js/jquery.scrollto.js"></script>
    <script src="<?=HTTP_SITE; ?>assets/js/owl.carousel.js"></script>
    <script src="<?=HTTP_SITE; ?>assets/js/jquery.countTo.js"></script>
    <script src="<?=HTTP_SITE; ?>assets/js/classie.js"></script>
    <script src="<?=HTTP_SITE; ?>assets/js/video.resize.js"></script>
    <script src="<?=HTTP_SITE; ?>assets/js/validation.js"></script>
    <script src="<?=HTTP_SITE; ?>assets/js/wow.min.js"></script>
    <script src="<?=HTTP_SITE; ?>assets/js/jquery.magnific-popup.min.js"></script>
    <script src="<?=HTTP_SITE; ?>assets/js/enquire.min.js"></script>
    <script src="<?=HTTP_SITE; ?>assets/js/designesia.js"></script>
    <script src="<?=HTTP_SITE; ?>assets/demo/demo.js"></script>

</body>
</html>
