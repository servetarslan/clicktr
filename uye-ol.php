<?
include("inc/config.php");
include("inc/fonk.php");
$ceks = mysql_fetch_array(mysql_query("select * from ayar where dil = 'tr'"));
$link = 'uye-ol';
$cek = mysql_fetch_array(mysql_query("select * from kategori where link = '$link' and tur = 'StaticPage' and durum = '1' and dil = 'tr'"));


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?=$cek["title"]?></title>
	<meta name="robots" content="index, follow" >
    <meta name="description" content="<?=$cek["description"]?>" />    
    <meta name="author" content="">
    <!--[if lt IE 9]>
	<script src="assets/js/html5shiv.js"></script>
	<![endif]-->


    <!-- CSS Files
    ================================================== -->
    <link rel="stylesheet" href="assets/css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="assets/css/jpreloader.css" type="text/css">
    <link rel="stylesheet" href="assets/css/animate.css" type="text/css">
    <link rel="stylesheet" href="assets/css/plugin.css" type="text/css">
    <link rel="stylesheet" href="assets/css/owl.carousel.css" type="text/css">
    <link rel="stylesheet" href="assets/css/owl.theme.css" type="text/css">
    <link rel="stylesheet" href="assets/css/owl.transitions.css" type="text/css">
    <link rel="stylesheet" href="assets/css/magnific-popup.css" type="text/css">
    <link rel="stylesheet" href="assets/css/style.css" type="text/css">
    <link rel="stylesheet" href="assets/demo/demo.css" type="text/css">

    <!-- custom background -->
    <link rel="stylesheet" href="assets/css/bg.css" type="text/css">

    <!-- color scheme -->
    <link rel="stylesheet" href="assets/css/color.css" type="text/css" id="colors">

    <!-- load fonts -->
    <link rel="stylesheet" href="assets/fonts/font-awesome/css/font-awesome.css" type="text/css">
    <link rel="stylesheet" href="assets/fonts/fontawesome/css/all.css" type="text/css">
    <link rel="stylesheet" href="assets/fonts/elegant_font/HTML_CSS/style.css" type="text/css">
    <link rel="stylesheet" href="assets/fonts/et-line-font/style.css" type="text/css">

    <!-- revolution slider -->
    <link rel="stylesheet" href="rs-plugin/css/settings.css" type="text/css">
    <link rel="stylesheet" href="assets/css/rev-settings.css" type="text/css">
</head>
<body class="page-about">

    <div id="wrapper">

    <?php include('header.php'); ?>
    
            <!-- content begin -->
            <div id="content">
               
    
                <div class="container">
                    <div class="row">
    
                        <div class="col-md-12">
                            <h2>Turkish Click'e Katıl</h2>
                            <form class="">
                                <div class="row form-content">
                                    <h4 class="col-md-12">Kişisel Bilgileriniz</h4>
                                    <div class="form-group col-md-6">
                                        <label>Adınız (*)</label>
                                        <input type="text" class="form-control" required >
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Soyadınız (*)</label>
                                        <input type="text" class="form-control" required >
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Email (*)</label>
                                        <input type="email" class="form-control" required >
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Telefon (*)</label>
                                        <input type="tel" class="form-control" required >
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Parola (*)</label>
                                        <input type="password" class="form-control" required>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Parola Tekrar (*)</label>
                                        <input type="password" class="form-control" required>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label>Ülke</label>
                                        <input type="text" class="form-control" >
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label>Sehir</label>
                                        <input type="text" class="form-control"  >
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label>Adres</label>
                                        <input type="text" class="form-control"  >
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label>Posta Kodu</label>
                                        <input type="text" class="form-control" required >
                                    </div>

                                    <h4 class="col-md-12">Ödeme Bilgileriniz</h4>

                                    <div class="form-group col-md-6">
                                        <label>T.C. Kimlik No (*)</label>
                                        <input type="text" class="form-control" required >
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label>Banka Adı (*)</label>
                                        <input type="text" class="form-control" required >
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label>Hesap No (*)</label>
                                        <input type="text" class="form-control" required >
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label>Şube No (*)</label>
                                        <input type="text" class="form-control" required >
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label>Hesap Sahibi Ad Soyad (*)</label>
                                        <input type="text" class="form-control" required >
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label>Iban (*)</label>
                                        <input type="text" class="form-control" required >
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label>Fatura Kesebiliyorum</label>
                                        <select name="favori-kanal" class="form-control">
                                            <option>Seçiniz</option>
                                            <option value="yes">Evet</option>
                                            <option value="no">Hayır</option>
                                            
                                        </select>
                                    </div>

                                </div>

                                <div class="row form-content social-content">
                                        <h4 class="col-md-12">İçerik Kanallarım</h4>
                                        <div class="form-group col-md-4">
                                            <label><i class="fab fa-facebook"></i></label>
                                            <input type="text" class="form-control" placeholder="Profil Url" >
                                        </div>

                                        <div class="form-group col-md-4">
                                            <label><i class="fab fa-youtube"></i></label>
                                            <input type="text" class="form-control" placeholder="Kanal Url" >
                                        </div>

                                        <div class="form-group col-md-4">
                                            <label><i class="fab fa-instagram"></i></label>
                                            <input type="text" class="form-control" placeholder="Kullanıcı Adı" >
                                        </div>

                                        <div class="form-group col-md-4">
                                            <label><i class="fab fa-snapchat-ghost"></i></label>
                                            <input type="text" class="form-control" placeholder="Kullnıcı Adı" >
                                        </div>

                                        <div class="form-group col-md-4">
                                            <label><i class="fab fa-telegram"></i></label>
                                            <input type="text" class="form-control" placeholder="Kullnıcı Adı" >
                                        </div>

                                        <div class="form-group col-md-4">
                                            <label><i class="fab fa-twitter"></i></label>
                                            <input type="text" class="form-control" placeholder="Kullanıcı Adı" >
                                        </div>

                                        <div class="form-group col-md-4">
                                            <label><i class="fab fa-whatsapp"></i></label>
                                            <input type="text" class="form-control" placeholder="Group Url" >
                                        </div>

                                        <div class="form-group col-md-4">
                                            <label><i class="fas fa-comment-dots"></i></label>
                                            <input type="text" class="form-control" placeholder="Blog Url" >
                                        </div>
                                    </div>

                                    <div class="row form-content social-content">
                                        <h4 class="col-md-12">Ülkeler</h4>
                                        <div class="form-group col-md-2">
                                            <input type="checkbox" class="styles-check" id="Turkiye">
                                            <label class="autos styles" for="Turkiye">Türkiye</label>
                                        </div>

                                        <div class="form-group col-md-2">
                                            <input type="checkbox" class="styles-check" id="Azerbaycan">
                                            <label class="autos styles" for="Azerbaycan">Azerbaycan</label>
                                        </div>

                                        <div class="form-group col-md-2">
                                            <input type="checkbox" class="styles-check" id="Kazakistan">
                                            <label class="autos styles" for="Kazakistan">Kazakistan</label>
                                        </div>

                                        <div class="form-group col-md-2">
                                            <input type="checkbox" class="styles-check" id="Kirgizistan">
                                            <label class="autos styles" for="Kirgizistan">Kırgızistan</label>
                                        </div>

                                        <div class="form-group col-md-2">
                                            <input type="checkbox" class="styles-check" id="Turkmenistan">
                                            <label class="autos styles" for="Turkmenistan">Türkmenistan</label>
                                        </div>

                                        <div class="form-group col-md-2">
                                            <input type="checkbox" class="styles-check" id="Ozbekistan">
                                            <label class="autos styles" for="Ozbekistan">Özbekistan</label>
                                        </div>

                                        <div class="form-group col-md-2">
                                            <input type="checkbox" class="styles-check" id="Gurcistan">
                                            <label class="autos styles" for="Gurcistan">Gürcistan</label>
                                        </div>

                                        <div class="form-group col-md-2">
                                            <input type="checkbox" class="styles-check" id="KKTC ">
                                            <label class="autos styles" for="KKTC ">KKTC </label>
                                        </div>

                                        <div class="form-group col-md-2">
                                            <input type="checkbox" class="styles-check" id="Diger">
                                            <label class="autos styles" for="Diger">Diğer</label>
                                        </div>
                                        
                                    </div>

                                    <div class="row form-content social-content abgne-menu-20140101-2">
                                        <h4 class="col-md-12">İlgi Alanları</h4>

                                        <div class="form-group col-md-2">
                                            <input class="maxCheck" type="checkbox" id="Araba" >
                                            <label for="Araba">Araba</label>
                                        </div>
                                        
                                        <div class="form-group col-md-2">
                                            <input class="maxCheck" type="checkbox" id="Guzellik" >
                                            <label for="Guzellik">Güzellik</label>
                                        </div>

                                        <div class="form-group col-md-2">
                                            <input class="maxCheck" type="checkbox" id="Elektronik" >
                                            <label for="Elektronik">Elektronik</label>
                                        </div>

                                        <div class="form-group col-md-2">
                                            <input class="maxCheck" type="checkbox" id="Kuponlar" >
                                            <label for="Kuponlar">Kuponlar</label>
                                        </div>

                                        <div class="form-group col-md-2">
                                            <input class="maxCheck" type="checkbox" id="Egitim" >
                                            <label for="Egitim">Egitim</label>
                                        </div>

                                        <div class="form-group col-md-2">
                                            <input class="maxCheck" type="checkbox" id="Eglence" >
                                            <label for="Eglence">Eğlence</label>
                                        </div>

                                        <div class="form-group col-md-2">
                                            <input class="maxCheck" type="checkbox" id="Aile" >
                                            <label for="Aile">Aile</label>
                                        </div>

                                       
                                        <div class="form-group col-md-2">
                                            <input class="maxCheck" type="checkbox" id="Moda" >
                                            <label for="Moda">Moda</label>
                                        </div>

                                        <div class="form-group col-md-2">
                                            <input class="maxCheck" type="checkbox" id="Yiyecek" >
                                            <label for="Yiyecek">Yiyecek</label>
                                        </div>

                                        <div class="form-group col-md-2">
                                            <input class="maxCheck" type="checkbox" id="Ascilik" >
                                            <label for="Ascilik">Aşçılık</label>
                                        </div>

                                        <div class="form-group col-md-2">
                                            <input class="maxCheck" type="checkbox" id="Oyun" >
                                            <label for="Oyun">Oyun</label>
                                        </div>

                                        <div class="form-group col-md-2">
                                            <input class="maxCheck" type="checkbox" id="Saglik" name="">
                                            <label for="Saglik">Sağlık</label>
                                        </div>

                                        <div class="form-group col-md-2">
                                            <input class="maxCheck" type="checkbox" id="Medikal" name="">
                                            <label for="Medikal">Medikal</label>
                                        </div>

                                        <div class="form-group col-md-2">
                                            <input class="maxCheck" type="checkbox" id="yasamtarzi" name="">
                                            <label for="yasamtarzi">Yaşam Tarzı</label>
                                        </div>

                                        <div class="form-group col-md-2">
                                            <input class="maxCheck" type="checkbox" id="Spor" name="">
                                            <label for="Spor">Spor</label>
                                        </div>

                                        <div class="form-group col-md-2">
                                            <input class="maxCheck" type="checkbox" id="Fitnes" name="">
                                            <label for="Fitnes">Fitnes</label>
                                        </div>

                                        <div class="form-group col-md-2">
                                            <input class="maxCheck" type="checkbox" id="Seyehat" name="">
                                            <label for="Seyehat">Seyehat</label>
                                        </div>
                                        
                                    </div>

                                    <div class="row form-content social-content">
                                        <div class="form-group form-check col-md-12">
                                            <input type="checkbox" class="form-check-input" id="sozlesme">
                                            <label class="form-check-label autos" for="sozlesme"><a href="#">Sözleşmeyi</a> Kabul Ediyorum.</label>
                                        </div>
                                        <div class="form-group form-check col-md-6">
                                            <input style="display: inline-block;" type="checkbox" class="form-check-input" id="gizlilik">
                                            <label style="display: contents;" class="form-check-label autos" for="gizlilik">Başvurumda yer alan kişisel verilerimin işleme alınması, üçüncü şahıslarla paylaşılması, değerlendirme, sahtekarlık önleme ve diğer yasa dışı faaliyetler için rıza gösteriyorum.</label>
                                        </div>
                                    </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">KAYIT OL</button>
                                </div>
                            </form>
                        </div>
    
               
                    </div>
                </div>
            </div>

            <?php include('footer.php'); ?>
    </div>
    <!-- style switcher
    ================================================== -->
    


    <!-- Javascript Files
    ================================================== -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/jpreLoader.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/jquery.isotope.min.js"></script>
    <script src="assets/js/easing.js"></script>
    <script src="assets/js/jquery.flexslider-min.js"></script>
    <script src="assets/js/jquery.scrollto.js"></script>
    <script src="assets/js/owl.carousel.js"></script>
    <script src="assets/js/jquery.countTo.js"></script>
    <script src="assets/js/classie.js"></script>
    <script src="assets/js/video.resize.js"></script>
    <script src="assets/js/validation.js"></script>
    <script src="assets/js/wow.min.js"></script>
    <script src="assets/js/jquery.magnific-popup.min.js"></script>
    <script src="assets/js/enquire.min.js"></script>
    <script src="assets/js/designesia.js"></script>
    <script src="assets/demo/demo.js"></script>

</body>
</html>
