/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';

	config.protectedSource.push(/<i[^>]*><\/i>/g);
    config.allowedContent  = 'img form input textarea h1 h2 h3 h4 h5 h6 h7 h8 i b nav param pre flash br a td p span font em strong section table tr th td style  script iframe u s li ul div[*]{*}(*)';// add every html element you'll use in your eml tempate,I don't test if '*[*]{*}(*) will work for all html tag?

   // I have  tested that *[*] pattern can't work!!! one hour after the first post!	config.allowedContent = 'img form input param pre flash br a td p span font em strong table tr th td style  script iframe u s li ul div[*]{*}(*)';
};
