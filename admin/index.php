<?php session_start(); ob_start();
include("../inc/config.php");
include("../inc/session.php");
Function baslangic(){

if($_SESSION["kullanici"] == 'srrvt' ){

	header("Location: bsvr.php");
}else{}


?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />

        <title>Yönetim Paneli</title>
        <!-- Link shortcut icon-->
        <link rel="shortcut icon" type="image/ico" href="images/favicon2.html" />
        <!-- Link css-->
        <link rel="stylesheet" type="text/css" href="css/zice.style.css"/>
        <link rel="stylesheet" type="text/css" href="css/icon.css"/>
        <link rel="stylesheet" type="text/css" href="css/ui-custom.css"/>
        <link rel="stylesheet" type="text/css" href="css/timepicker.css"  />
        <link rel="stylesheet" type="text/css" href="components/colorpicker/css/colorpicker.css"  />
        <link rel="stylesheet" type="text/css" href="components/elfinder/css/elfinder.css" />
        <link rel="stylesheet" type="text/css" href="components/datatables/dataTables.css"  />
        <link rel="stylesheet" type="text/css" href="components/validationEngine/validationEngine.jquery.css" />

        <link rel="stylesheet" type="text/css" href="components/jscrollpane/jscrollpane.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="components/fancybox/jquery.fancybox.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="components/tipsy/tipsy.css" media="all" />
        <link rel="stylesheet" type="text/css" href="components/editor/jquery.cleditor.css"  />
        <link rel="stylesheet" type="text/css" href="components/chosen/chosen.css" />
        <link rel="stylesheet" type="text/css" href="components/confirm/jquery.confirm.css" />
        <link rel="stylesheet" type="text/css" href="components/sourcerer/sourcerer.css"/>
        <link rel="stylesheet" type="text/css" href="components/fullcalendar/fullcalendar.css"/>
        <link rel="stylesheet" type="text/css" href="components/Jcrop/jquery.Jcrop.css"  />


        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="components/flot/excanvas.min.js"></script><![endif]-->

        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="components/ui/jquery.ui.min.js"></script>
        <script type="text/javascript" src="components/ui/jquery.autotab.js"></script>
        <script type="text/javascript" src="components/ui/timepicker.js"></script>
        <script type="text/javascript" src="components/colorpicker/js/colorpicker.js"></script>
        <script type="text/javascript" src="components/checkboxes/iphone.check.js"></script>
        <script type="text/javascript" src="components/elfinder/js/elfinder.full.js"></script>
        <script type="text/javascript" src="components/datatables/dataTables.min.js"></script>
        <script type="text/javascript" src="components/scrolltop/scrolltopcontrol.js"></script>
        <script type="text/javascript" src="components/fancybox/jquery.fancybox.js"></script>
        <script type="text/javascript" src="components/jscrollpane/mousewheel.js"></script>
        <script type="text/javascript" src="components/jscrollpane/mwheelIntent.js"></script>
        <script type="text/javascript" src="components/jscrollpane/jscrollpane.min.js"></script>
        <script type="text/javascript" src="components/spinner/ui.spinner.js"></script>
        <script type="text/javascript" src="components/tipsy/jquery.tipsy.js"></script>
        <script type="text/javascript" src="components/editor/jquery.cleditor.js"></script>
        <script type="text/javascript" src="components/chosen/chosen.js"></script>
        <script type="text/javascript" src="components/confirm/jquery.confirm.js"></script>
        <script type="text/javascript" src="components/validationEngine/jquery.validationEngine.js" ></script>
        <script type="text/javascript" src="components/validationEngine/jquery.validationEngine-en.js" ></script>
        <script type="text/javascript" src="components/vticker/jquery.vticker-min.js"></script>
        <script type="text/javascript" src="components/sourcerer/sourcerer.js"></script>
        <script type="text/javascript" src="components/fullcalendar/fullcalendar.js"></script>
        <script type="text/javascript" src="components/flot/flot.js"></script>
        <script type="text/javascript" src="components/flot/flot.pie.min.js"></script>
        <script type="text/javascript" src="components/flot/flot.resize.min.js"></script>
        <script type="text/javascript" src="components/flot/graphtable.js"></script>

        <script type="text/javascript" src="components/uploadify/swfobject.js"></script>
        <script type="text/javascript" src="components/uploadify/uploadify.js"></script>
        <script type="text/javascript" src="components/checkboxes/customInput.jquery.js"></script>
        <script type="text/javascript" src="components/effect/jquery-jrumble.js"></script>
        <script type="text/javascript" src="components/filestyle/jquery.filestyle.js" ></script>
        <script type="text/javascript" src="components/placeholder/jquery.placeholder.js" ></script>
		<script type="text/javascript" src="components/Jcrop/jquery.Jcrop.js" ></script>
        <script type="text/javascript" src="components/imgTransform/jquery.transform.js" ></script>
        <script type="text/javascript" src="components/webcam/webcam.js" ></script>
		<script type="text/javascript" src="components/rating_star/rating_star.js"></script>
		<script type="text/javascript" src="components/dualListBox/dualListBox.js"  ></script>
		<script type="text/javascript" src="components/smartWizard/jquery.smartWizard.min.js"></script>
        <script type="text/javascript" src="js/jquery.cookie.js"></script>
        <script type="text/javascript" src="js/zice.custom.js"></script>

        </head>
        <body class="dashborad">
        <div id="alertMessage" class="error"></div>


		 <!--//  header -->
					   <?php include("header.php") ?>

         <!--//  header -->


			<div id="shadowhead"></div>
            <div id="hide_panel">
                  <a class="butAcc" rel="0" id="show_menu"></a>
                  <a class="butAcc" rel="1" id="hide_menu"></a>
                  <a class="butAcc" rel="0" id="show_menu_icon"></a>
                  <a class="butAcc" rel="1" id="hide_menu_icon"></a>
            </div>

                  <div id="left_menu">
                    <!--//  menu starts -->
					   <?php include("menu.php") ?>

					<!--//  menu finish -->
					</div>


            <div id="content">
                <div class="inner">
					<div class="topcolumn">
						<div class="logo"></div>



						   <!--//  menu starts -->

							<?php include("fastmenu.php") ?>

							<!--//  menu finish -->



					</div>
                    <div class="clear"></div>

                    <div class="onecolumn" >
                        <div class="header"> <span ><span class="ico gray home"></span>GENEL SİTE AYARLARI</span> </div>
                        <div class="clear"></div>
                        <div class="content" >
                            <div class="boxtitle min">AYARLAR</div>
                              <div  class="grid2" style="border:1px solid #666; padding-right:10px; margin-left:-15px;">
                                <div class="inner">
                                    <form action="index.php?Git=ekle" enctype="multipart/form-data" method="post">

								<?php $yayar = mysql_fetch_array(mysql_query("select * from ayar where dil = 'tr'")); ?>
										<div style="color:red; font-weight:bold; margin-bottom:10px;"><img src="images/icon/tr.png" width="25"> GENEL AYARLAR</div>
										<div class="section">
                                            <label> Site Adı <small>Seo</small></label>
                                            <div>
                                                <input type="text" name="ad"   class=" full"  value="<?php echo stripslashes(trim($yayar["ad"])) ?>" />
                                            </div>
                                        </div>



										<div class="section">
                                            <label> Title (Başlık) <small>Seo</small></label>
                                            <div>
                                                <input type="text" name="title"   class=" full"  value="<?php echo stripslashes(trim($yayar["title"])) ?>" />
                                            </div>
                                        </div>


										<div class="section">
                                            <label> Keyword (Anahtar Kelimeler) <small>Seo</small></label>
                                            <div>
                                                <textarea name="keyword"   class=" full" > <?php echo stripslashes(trim($yayar["keyword"])) ?> </textarea>
                                            </div>
                                        </div>


										<div class="section">
                                            <label> Description (Açıklama) <small>Seo</small></label>
                                            <div>
                                                <textarea name="description"   class=" full" > <?php echo stripslashes(trim($yayar["description"])) ?> </textarea>
                                            </div>
                                        </div>


										<div class="section">
                                            <label>Site Açıklaması<small></small></label>
                                            <div>
                                                <textarea name="fouter"   class=" full" > <?php echo stripslashes(trim($yayar["fouter"])); ?> </textarea>
                                            </div>
                                        </div>

                                        <div class="section">
                                            <label>Adres<small></small></label>
                                            <div>
                                                <textarea name="address"   class=" full" > <?php echo stripslashes(trim($yayar["address"])); ?> </textarea>
                                            </div>
                                        </div>

                                        <div class="section">
                                            <label>Maps<small></small></label>
                                            <div>
                                                <textarea name="maps"   class=" full" > <?php echo trim($yayar["maps"]); ?> </textarea>
                                            </div>
                                        </div>

										<? if($yayar["kare"] != ''){$image = $yayar["kare"];}else{$image = 'no.jpg';} ?>
										<div class="section ">
										<label>Var Olan Logo <small></small></label>
										<div>
											<img src="../statics/kare/<?php echo $image; ?>" style="border:1px solid #777;">
										</div>
										</div>

										<div class="section ">
										<label> Logo <small>Resim Yükle</small></label>
										<div>
											<input type="file" name="resim" class="fileupload" />
                                            <span class="f_help">195px * 50px png</span>
										</div>
										</div>


										<div class="section">
                                            <label> Analystic (Google) <small>Seo</small></label>
                                            <div>
                                                <input type="text" name="analistic"   class=" full"  value="<?php echo trim($yayar["analistic"]); ?>" />
                                            </div>
                                        </div>



										<div class="section">
                                            <label> Telefon <small>Genel</small></label>
                                            <div>
                                                <input type="text" name="tel"   class=" full"  value="<?php echo stripslashes(trim($yayar["tel"])) ?>" />
                                            </div>
                                        </div>

                                        <div class="section">
                                            <label> Facebook<small>Sosyal Medya</small></label>
                                            <div>
                                                <input type="text" name="facebook"   class=" full"  value="<?php echo stripslashes(trim($yayar["facebook"])) ?>" />
                                            </div>
                                        </div>



                                        <div class="section">
                                            <label> Instagram<small>Sosyal Medya</small></label>
                                            <div>
                                                <input type="text" name="twitter"   class=" full"  value="<?php echo stripslashes(trim($yayar["twitter"])) ?>" />
                                            </div>
                                        </div>

                                        <div class="section">
                                            <label> Lınkedin<small>Sosyal Medya</small></label>
                                            <div>
                                                <input type="text" name="linkedin"   class=" full"  value="<?php echo stripslashes(trim($yayar["linkedin"])) ?>" />
                                            </div>
                                        </div>

                                        <div class="section">
                                            <label> Appstore<small>Mobile App</small></label>
                                            <div>
                                                <input type="text" name="google"   class=" full"  value="<?php echo trim($yayar["google"]) ?>" />
                                            </div>
                                        </div>



                                        <div class="section">
                                            <label> Google Play <small>Mobile App</small></label>
                                            <div>
                                                <input type="text" name="youtube"   class=" full"  value="<?php echo stripslashes(trim($yayar["youtube"])) ?>" />
                                            </div>
                                        </div>

										<div class="section">
                                            <label> Email <small>Genel</small></label>
                                            <div>
                                                <input type="text" name="mail"   class=" full"  value="<?php echo stripslashes(trim($yayar["mail"])) ?>" />
                                            </div>
                                        </div>


										<div class="section">
                                            <label>Site Yöneticisi <small>Genel</small></label>
                                            <div>
                                                <input type="text" name="admin"   class=" full"  value="<?php echo stripslashes(trim($yayar["admin"])) ?>" />
                                            </div>
                                        </div>

                                        <div class="section last">
                                            <div>
											<input type="submit" class="uibutton loading"  title="Kaydet" value="Kaydet" rel="1" />
											</div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <div  class="grid2" style="border:1px solid #666; padding-right:10px; margin-left:-15px;">
                                <div class="inner">
                                    <form action="index.php?Git=ekleen" enctype="multipart/form-data" method="post">

								    <?php $yayar = mysql_fetch_array(mysql_query("select * from ayar where dil = 'en'")); ?>
										<div style="color:red; font-weight:bold; margin-bottom:10px;"><img src="images/icon/en.png" width="25"> GENEL AYARLAR</div>
										<div class="section">
                                            <label> Site Adı <small>Seo</small></label>
                                            <div>
                                                <input type="text" name="ad"   class=" full"  value="<?php echo stripslashes(trim($yayar["ad"])) ?>" />
                                            </div>
                                        </div>



										<div class="section">
                                            <label> Title (Başlık) <small>Seo</small></label>
                                            <div>
                                                <input type="text" name="title"   class=" full"  value="<?php echo stripslashes(trim($yayar["title"])) ?>" />
                                            </div>
                                        </div>


										<div class="section">
                                            <label> Keyword (Anahtar Kelimeler) <small>Seo</small></label>
                                            <div>
                                                <textarea name="keyword"   class=" full" > <?php echo stripslashes(trim($yayar["keyword"])) ?> </textarea>
                                            </div>
                                        </div>


										<div class="section">
                                            <label> Description (Açıklama) <small>Seo</small></label>
                                            <div>
                                                <textarea name="description"   class=" full" > <?php echo stripslashes(trim($yayar["description"])) ?> </textarea>
                                            </div>
                                        </div>


										<div class="section">
                                            <label>Site Açıklaması<small></small></label>
                                            <div>
                                                <textarea name="fouter"   class=" full" > <?php echo stripslashes(trim($yayar["fouter"])); ?> </textarea>
                                            </div>
                                        </div>

                                        <div class="section">
                                            <label>Adres<small></small></label>
                                            <div>
                                                <textarea name="address"   class=" full" > <?php echo stripslashes(trim($yayar["address"])); ?> </textarea>
                                            </div>
                                        </div>

                                        <div class="section">
                                            <label>Maps<small></small></label>
                                            <div>
                                                <textarea name="maps"   class=" full" > <?php echo trim($yayar["maps"]); ?> </textarea>
                                            </div>
                                        </div>

										<? if($yayar["kare"] != ''){$image = $yayar["kare"];}else{$image = 'no.jpg';} ?>
										<div class="section ">
										<label>Var Olan Logo <small></small></label>
										<div>
											<img src="../statics/kare/<?php echo $image; ?>" style="border:1px solid #777;">
										</div>
										</div>

										<div class="section ">
										<label> Logo <small>Resim Yükle</small></label>
										<div>
											<input type="file" name="resim" class="fileupload" />
                                            <span class="f_help">195px * 50px png</span>
										</div>
										</div>


										<div class="section">
                                            <label> Analystic (Google) <small>Seo</small></label>
                                            <div>
                                                <input type="text" name="analistic"   class=" full"  value="<?php echo trim($yayar["analistic"]); ?>" />
                                            </div>
                                        </div>



										<div class="section">
                                            <label> Telefon <small>Genel</small></label>
                                            <div>
                                                <input type="text" name="tel"   class=" full"  value="<?php echo stripslashes(trim($yayar["tel"])) ?>" />
                                            </div>
                                        </div>

                                        <div class="section">
                                            <label> Facebook<small>Sosyal Medya</small></label>
                                            <div>
                                                <input type="text" name="facebook"   class=" full"  value="<?php echo stripslashes(trim($yayar["facebook"])) ?>" />
                                            </div>
                                        </div>



                                        <div class="section">
                                            <label> Instagram<small>Sosyal Medya</small></label>
                                            <div>
                                                <input type="text" name="twitter"   class=" full"  value="<?php echo stripslashes(trim($yayar["twitter"])) ?>" />
                                            </div>
                                        </div>

                                        <div class="section">
                                            <label> Lınkedin<small>Sosyal Medya</small></label>
                                            <div>
                                                <input type="text" name="linkedin"   class=" full"  value="<?php echo stripslashes(trim($yayar["linkedin"])) ?>" />
                                            </div>
                                        </div>

                                        <div class="section">
                                            <label> Appstore<small>Mobile App</small></label>
                                            <div>
                                                <input type="text" name="google"   class=" full"  value="<?php echo trim($yayar["google"]) ?>" />
                                            </div>
                                        </div>



                                        <div class="section">
                                            <label> Google Play <small>Mobile App</small></label>
                                            <div>
                                                <input type="text" name="youtube"   class=" full"  value="<?php echo stripslashes(trim($yayar["youtube"])) ?>" />
                                            </div>
                                        </div>

										<div class="section">
                                            <label> Email <small>Genel</small></label>
                                            <div>
                                                <input type="text" name="mail"   class=" full"  value="<?php echo stripslashes(trim($yayar["mail"])) ?>" />
                                            </div>
                                        </div>


										<div class="section">
                                            <label>Site Yöneticisi <small>Genel</small></label>
                                            <div>
                                                <input type="text" name="admin"   class=" full"  value="<?php echo stripslashes(trim($yayar["admin"])) ?>" />
                                            </div>
                                        </div>

                                        <div class="section last">
                                            <div>

											<input type="submit" class="uibutton loading"  title="Kaydet" value="Kaydet" rel="1" />



											</div>
                                        </div>
                                    </form>
                                </div>
                            </div>





                            <div class="clear"></div>
                        </div>
                    </div>
                    <!-- // End onecolumn -->


                    <!-- // End onecolumn -->

                    <!--// two column window -->





					<!--//  Fouter starts -->

					   <?php include("fouter.php") ?>

					<!--//  Fouter finish -->




			   </div> <!--// End inner -->
            </div> <!--// End content -->
</body>
</html>

<?php
}

Function ekle(){

$ad 				= addslashes(trim($_POST["ad"]));
$title 				= addslashes(trim($_POST["title"]));
$keyword 			= addslashes(trim($_POST["keyword"]));
$aciklama 			= trim($_POST["aciklama"]);
$description 		= addslashes(trim($_POST["description"]));
$fouter 			= addslashes(trim($_POST["fouter"]));
$address 			= addslashes(trim($_POST["address"]));
$analistic 			= addslashes(trim($_POST["analistic"]));
$mail 				= addslashes(trim($_POST["mail"]));
$maps 				= trim($_POST["maps"]);
$tel 				= addslashes(trim($_POST["tel"]));
$admin 				= addslashes(trim($_POST["admin"]));
$facebook			= (trim($_POST["facebook"]));
$twitter			= (trim($_POST["twitter"]));
$google				= trim($_POST["google"]);
$linkedin			= (trim($_POST["linkedin"]));
$youtube			= (trim($_POST["youtube"]));

	if(@$_FILES["resim"]["type"] != ""){
	$foo = new Upload($_FILES['resim']);
		if ($foo->uploaded){
		echo "Yukleniyor...";



			$foo->Process(DIR_RESIM);
			if ($foo->processed){
			$resim = $foo->file_dst_name;

						if ($foo->uploaded){
						// $foo->file_new_name_body = 'galeri';
							$foo->allowed = array('image/*');
							$foo->Process(DIR_KUCUK);
						if ($foo->processed){
							$kucuk = $foo->file_dst_name;

						if ($foo->uploaded){
						// $foo->file_new_name_body = 'galeri';
							$foo->allowed = array('image/*');
							$foo->image_resize          = true;
							$foo->image_ratio_fill      = true;
							$foo->image_y               = 100;
							$foo->image_x               = 100;
							$foo->Process(DIR_KARE);
						if ($foo->processed){
							$kare = $foo->file_dst_name;

$ekle = mysql_query("update ayar set ad='$ad', title='$title',address='$address',resim='$resim',kucuk='$kucuk',kare='$kare', keyword='$keyword',aciklama='$aciklama', description='$description', fouter='$fouter', maps='$maps', analistic='$analistic', facebook='$facebook', twitter='$twitter', google='$google', linkedin='$linkedin', youtube='$youtube', flickr='$flickr', mail='$mail', tel='$tel', admin='$admin', durum='$statu' where dil = 'tr'");

if($ekle){ header("Location:index.php");}

}}}}}}}else{
$ekle = mysql_query("update ayar set ad='$ad', title='$title', address='$address', keyword='$keyword',aciklama='$aciklama', description='$description', fouter='$fouter', maps='$maps', analistic='$analistic', facebook='$facebook', twitter='$twitter', google='$google', linkedin='$linkedin', youtube='$youtube', flickr='$flickr', mail='$mail', tel='$tel', admin='$admin', durum='$statu' where dil = 'tr'");

if($ekle){ header("Location:index.php");}

}}




Function ekleen(){

    $ad 				= addslashes(trim($_POST["ad"]));
    $title 				= addslashes(trim($_POST["title"]));
    $keyword 			= addslashes(trim($_POST["keyword"]));
    $aciklama 			= trim($_POST["aciklama"]);
    $description 		= addslashes(trim($_POST["description"]));
    $fouter 			= addslashes(trim($_POST["fouter"]));
    $address 			= addslashes(trim($_POST["address"]));
    $analistic 			= addslashes(trim($_POST["analistic"]));
    $mail 				= addslashes(trim($_POST["mail"]));
    $maps 				= trim($_POST["maps"]);
    $tel 				= addslashes(trim($_POST["tel"]));
    $admin 				= addslashes(trim($_POST["admin"]));
    $facebook			= (trim($_POST["facebook"]));
    $twitter			= (trim($_POST["twitter"]));
    $google				= trim($_POST["google"]);
    $linkedin			= (trim($_POST["linkedin"]));
    $youtube			= (trim($_POST["youtube"]));
    
        if(@$_FILES["resim"]["type"] != ""){
        $foo = new Upload($_FILES['resim']);
            if ($foo->uploaded){
            echo "Yukleniyor...";
    
    
    
                $foo->Process(DIR_RESIM);
                if ($foo->processed){
                $resim = $foo->file_dst_name;
    
                            if ($foo->uploaded){
                            // $foo->file_new_name_body = 'galeri';
                                $foo->allowed = array('image/*');
                                $foo->Process(DIR_KUCUK);
                            if ($foo->processed){
                                $kucuk = $foo->file_dst_name;
    
                            if ($foo->uploaded){
                            // $foo->file_new_name_body = 'galeri';
                                $foo->allowed = array('image/*');
                                $foo->image_resize          = true;
                                $foo->image_ratio_fill      = true;
                                $foo->image_y               = 100;
                                $foo->image_x               = 100;
                                $foo->Process(DIR_KARE);
                            if ($foo->processed){
                                $kare = $foo->file_dst_name;
    
    $ekle = mysql_query("update ayar set ad='$ad', title='$title',address='$address',resim='$resim',kucuk='$kucuk',kare='$kare', keyword='$keyword',aciklama='$aciklama', description='$description', fouter='$fouter', maps='$maps', analistic='$analistic', facebook='$facebook', twitter='$twitter', google='$google', linkedin='$linkedin', youtube='$youtube', flickr='$flickr', mail='$mail', tel='$tel', admin='$admin', durum='$statu' where dil = 'en'");
    
    if($ekle){ header("Location:index.php");}
    
    }}}}}}}else{
    $ekle = mysql_query("update ayar set ad='$ad', title='$title', address='$address', keyword='$keyword',aciklama='$aciklama', description='$description', fouter='$fouter', maps='$maps', analistic='$analistic', facebook='$facebook', twitter='$twitter', google='$google', linkedin='$linkedin', youtube='$youtube', flickr='$flickr', mail='$mail', tel='$tel', admin='$admin', durum='$statu' where dil = 'en'");
    
    if($ekle){ header("Location:index.php");}
    
    }}
    



$Git = $_GET["Git"];
switch($Git){

	default:
	baslangic();
	break;

	 case "ekle":
     ekle();
	 break;


	 case "ekleen":
     ekleen();
	 break;



}


 ?>
