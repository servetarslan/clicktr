<?php /** Coder : Servet Arslan - Email : askardanadam@gmail.com - Phone : +90 544 719 02 50 **/
session_start(); ob_start();
include("../inc/config.php");
include("../inc/session.php");
include("../inc/fonk.php");

Function baslangic(){
global $uye;
$types = "S_liste";
$id = intval($_GET["id"]);
$tur = strip_tags($_GET["tur"]);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />

        <title>Yönetim Paneli</title>
        <!-- Link shortcut icon-->
        <link rel="shortcut icon" type="image/ico" href="images/favicon2.html" />
        <!-- Link css-->
        <link rel="stylesheet" type="text/css" href="css/zice.style.css"/>
        <link rel="stylesheet" type="text/css" href="css/icon.css"/>
        <link rel="stylesheet" type="text/css" href="css/ui-custom.css"/>
        <link rel="stylesheet" type="text/css" href="css/timepicker.css"  />
        <link rel="stylesheet" type="text/css" href="components/colorpicker/css/colorpicker.css"  />
        <link rel="stylesheet" type="text/css" href="components/elfinder/css/elfinder.css" />
        <link rel="stylesheet" type="text/css" href="components/datatables/dataTables.css"  />
        <link rel="stylesheet" type="text/css" href="components/validationEngine/validationEngine.jquery.css" />

        <link rel="stylesheet" type="text/css" href="components/jscrollpane/jscrollpane.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="components/fancybox/jquery.fancybox.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="components/tipsy/tipsy.css" media="all" />
        <link rel="stylesheet" type="text/css" href="components/editor/jquery.cleditor.css"  />
        <link rel="stylesheet" type="text/css" href="components/chosen/chosen.css" />
        <link rel="stylesheet" type="text/css" href="components/confirm/jquery.confirm.css" />
        <link rel="stylesheet" type="text/css" href="components/sourcerer/sourcerer.css"/>
        <link rel="stylesheet" type="text/css" href="components/fullcalendar/fullcalendar.css"/>
        <link rel="stylesheet" type="text/css" href="components/Jcrop/jquery.Jcrop.css"  />


        <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="components/flot/excanvas.min.js"></script><![endif]-->




        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="components/ui/jquery.ui.min.js"></script>
        <script type="text/javascript" src="components/ui/jquery.autotab.js"></script>
        <script type="text/javascript" src="components/ui/timepicker.js"></script>
        <script type="text/javascript" src="components/colorpicker/js/colorpicker.js"></script>
        <script type="text/javascript" src="components/checkboxes/iphone.check.js"></script>
        <script type="text/javascript" src="components/elfinder/js/elfinder.full.js"></script>
        <script type="text/javascript" src="components/datatables/dataTables.min.js"></script>
        <script type="text/javascript" src="components/datatables/ColVis.js"></script>
        <script type="text/javascript" src="components/scrolltop/scrolltopcontrol.js"></script>
        <script type="text/javascript" src="components/fancybox/jquery.fancybox.js"></script>
        <script type="text/javascript" src="components/jscrollpane/mousewheel.js"></script>
        <script type="text/javascript" src="components/jscrollpane/mwheelIntent.js"></script>
        <script type="text/javascript" src="components/jscrollpane/jscrollpane.min.js"></script>
        <script type="text/javascript" src="components/spinner/ui.spinner.js"></script>
        <script type="text/javascript" src="components/tipsy/jquery.tipsy.js"></script>
        <script type="text/javascript" src="components/editor/jquery.cleditor.js"></script>
        <script type="text/javascript" src="components/chosen/chosen.js"></script>
        <script type="text/javascript" src="components/confirm/jquery.confirm.js"></script>
        <script type="text/javascript" src="components/validationEngine/jquery.validationEngine.js" ></script>
        <script type="text/javascript" src="components/validationEngine/jquery.validationEngine-en.js" ></script>
        <script type="text/javascript" src="components/vticker/jquery.vticker-min.js"></script>
        <script type="text/javascript" src="components/sourcerer/sourcerer.js"></script>
        <script type="text/javascript" src="components/fullcalendar/fullcalendar.js"></script>
        <script type="text/javascript" src="components/flot/flot.js"></script>
        <script type="text/javascript" src="components/flot/flot.pie.min.js"></script>
        <script type="text/javascript" src="components/flot/flot.resize.min.js"></script>
        <script type="text/javascript" src="components/flot/graphtable.js"></script>

        <script type="text/javascript" src="components/uploadify/swfobject.js"></script>
        <script type="text/javascript" src="components/uploadify/uploadify.js"></script>
        <script type="text/javascript" src="components/checkboxes/customInput.jquery.js"></script>
        <script type="text/javascript" src="components/effect/jquery-jrumble.js"></script>
        <script type="text/javascript" src="components/filestyle/jquery.filestyle.js" ></script>
        <script type="text/javascript" src="components/placeholder/jquery.placeholder.js" ></script>
		<script type="text/javascript" src="components/Jcrop/jquery.Jcrop.js" ></script>
        <script type="text/javascript" src="components/imgTransform/jquery.transform.js" ></script>
        <script type="text/javascript" src="components/webcam/webcam.js" ></script>
		<script type="text/javascript" src="components/rating_star/rating_star.js"></script>
		<script type="text/javascript" src="components/dualListBox/dualListBox.js"  ></script>
		<script type="text/javascript" src="components/smartWizard/jquery.smartWizard.min.js"></script>
        <script type="text/javascript" src="js/jquery.cookie.js"></script>
        <script type="text/javascript" src="js/zice.custom.js"></script>
		<script src="ckeditor/ckeditor.js" type="text/javascript"></script>




        </head>
        <body class="dashborad">
        <div id="alertMessage" class="error"></div>




        <!--//  header -->
					   <?php include("header.php") ?>

         <!--//  header -->




			<div id="shadowhead"></div>
            <div id="hide_panel">
                  <a class="butAcc" rel="0" id="show_menu"></a>
                  <a class="butAcc" rel="1" id="hide_menu"></a>
                  <a class="butAcc" rel="0" id="show_menu_icon"></a>
                  <a class="butAcc" rel="1" id="hide_menu_icon"></a>
            </div>

                  <div id="left_menu">
                      <!--//  menu starts -->
					   <?php include("menu.php") ?>

					<!--//  menu finish -->
              </div>


            <div id="content">
                <div class="inner">
					<div class="topcolumn">
                            <!--//  menu starts -->

							<?php include("fastmenu.php") ?>

							<!--//  menu finish -->
					</div>
                    <div class="clear"></div>

                        <div class="onecolumn" >
                        <div class="header"><span><span class="ico gray window"></span><?= strtoupper($tur)?> DÜZENLE</span></div>
                        <div class="clear"></div>
                        <div class="content" >

						<? $cek = mysql_fetch_array(mysql_query("select * from kategori where id = '".$id."'"));

						?>

                        <form action="pk_edit.php?Git=ekle&id=<?=$id;?>&tur=<?=$tur; ?>" enctype="multipart/form-data" method="post" id="demo">

                              <div class="section">
                                  <label> Başlık  <small>Başlık Bölümü</small></label>
                                  <div> <input type="text" name="ad" value="<?=$cek["ad"]?>" class=" large" /><span class="f_help"> </span></div>
                             </div>

                              <div class="section">
                                  <label> Title  <small>SEO Başlık Bölümü</small></label>
                                  <div> <input type="text" name="title" value="<?=$cek["title"]?>" class=" large" /><span class="f_help"> </span></div>
                             </div>


                             <? if( $tur != "Home"){ ?>
                              <div class="section">
                                  <label>URL Adresi <small>Yönlendirmek istediğiniz adres</small></label>
                                  <div> <input type="text" name="link" value="<?=$cek["link"]?>"  class=" large" /><span class="f_help">Lütfen; <strong style="color:red;">ğ-ş-ü-ç-ö-@-/-?-*-^-%-'-!-£-$-(-)-=-,-:-;->-<-é-+-½-}-{ .html, .php</strong> vb. karakterler Kullanmayınız.</span></div>
                             </div>
                            <? }?>

                            <? if($tur == 'Slider' || $tur == "Client" || $tur == "Menu"){ ?>
                              <div class="section">
                                  <label> Sıra  <small>Sıralama</small></label>
                                  <div> <input type="text" name="sira" value="<?=$cek["sira"]?>" class=" small" /><span class="f_help">Listeleme Sırası</span></div>
                             </div>
							 <? } ?>
				
                             <?  if($tur == 'Page'){?>
                              <div class="section">
                                  <label> Video  <small></small></label>
                                  <div> <input type="text" name="award" value="<?=$cek["award"];?>" class=" medium" /><span class="f_help"></span></div>
                             </div>
                             <? } ?>
	
                             <?  if($tur == 'Slider'){?>
                              <div class="section">
                                  <label> Buton Text  <small></small></label>
                                  <div> <input type="text" name="artdirector" value="<?=$cek["artdirector"];?>" class=" medium" /><span class="f_help"></span></div>
                             </div>
                             <? } ?>

                             <?  if($tur == 'Pr'){?>
                             <div class="section">
                                  <label> Kreatif Direktör  <small></small></label>
                                  <div> <input type="text" name="kreatif"  value="<?=$cek["kreatif"];?>" class=" medium" /><span class="f_help"></span></div>
                             </div>
							 <div class="section">
                                  <label> Yazar  <small></small></label>
                                  <div> <input type="text" name="yazar"  value="<?=$cek["yazar"];?>" class=" medium" /><span class="f_help"></span></div>
                             </div>
							<div class="section">
                                  <label> Fotoğrafçı  <small></small></label>
                                  <div> <input type="text" name="fotograf" value="<?=$cek["fotograf"];?>" class=" medium" /><span class="f_help"></span></div>
                             </div>
							 <div class="section">
                                  <label> Illistrator  <small></small></label>
                                  <div> <input type="text" name="illistrator" value="<?=$cek["illistrator"];?>" class=" medium" /><span class="f_help"></span></div>
                             </div>

							 <div class="section">
                                  <label> Art Direktör  <small></small></label>
                                  <div> <input type="text" name="artdirector"  value="<?=$cek["artdirector"];?>" class=" medium" /><span class="f_help"></span></div>
                             </div>
							 <div class="section">
                                  <label> ReTouch  <small></small></label>
                                  <div> <input type="text" name="retouch"  value="<?=$cek["retouch"];?>" class=" medium" /><span class="f_help"></span></div>
                             </div>
							 <div class="section">
                                  <label> Client  <small></small></label>
                                  <div> <input type="text" name="client"  value="<?=$cek["client"];?>" class=" medium" /><span class="f_help"></span></div>
                             </div>
							 <div class="section">
                                  <label> Ex Creative Director  <small></small></label>
                                  <div> <input type="text" name="excreative"  value="<?=$cek["excreative"];?>" class=" medium" /><span class="f_help"></span></div>
                             </div>
							 <div class="section">
                                  <label> Awards  <small></small></label>
                                  <div> <textarea  name="award"   class=" large" ><?=$cek["award"]?></textarea><span class="f_help"></span></div>
                             </div>
							 
							 <? } ?>
							 
							 <? if($tur == 'Menu') {?>
							  <?if($cek["menu"] == 1){$checked = 'checked';}?>
							  <? if($tur!= 'Menu'){$kelime = 'Anasayfa';}else{$kelime='Üst Menü';}?>

							 <div class="section">
                                  <label> <?=$kelime;?>   <small><?=$kelime;?> de yer alsın</small></label>
                                  <div> <input type="checkbox" class="on_off_checkbox" name="menu" value="1" <?=$checked;?> /><span class="f_help">Evet / Hayır</span></div>
                             </div>
							 <? } ?>

							 
							<? if($tur == 'Project'){
								$thisKat = $cek["kategori"];
								if($tur =='Referans'){$katGrubu = 'Odul_category'; $katType='';$katName='kategori';}
								else if($tur == 'Project'){$katGrubu = 'Category';$katType='multiple';$katName='kategori[]';}
								if($tur != 'Project'){$katWhere = "and id = '$thisKat'";}
							?>
							<div class="section">
                              <label>Kategori<small>Kategori Seçiniz</small></label>
                              <div>
                              	  <select data-placeholder="Seçiniz" name="<?=$katName;?>" <?=$katType;?> class="chzn-select" tabindex="2" >

								  <? $ceksi = mysql_query("select * from kategori where tur = '$katGrubu' $katwhere  order by sira asc");
										while($yazsi = mysql_fetch_array($ceksi)){
											$yzsId = $yazsi["id"];
										if($tur == 'Project'){
									  $sayKat = mysql_num_rows(mysql_query("select * from multicat where post = '$id' and cat = '$yzsId'"));
									  if($sayKat > 0){$slc = 'selected="selected"';}else{ $slc = '';}
										}
								  ?>
                                  <option value="<?=$yzsId;?>" <?=$slc;?>><?=$yazsi["ad"]?></option>
								  <? } ?>
								  
								  <?  if($tur == 'Project'){
									  
									  $ceksi = mysql_query("SELECT kategori.id, kategori.ad FROM kategori INNER JOIN multicat ON kategori.id = multicat.cat where tur = '$katGrubu' and kategori.id != multicat.cat order by id asc");
										while($yazsi = mysql_fetch_array($ceksi)){
								  ?>
                                  <option value="<?=$yazsi["id"]?>"><?=$yazsi["ad"]?></option>
								  <? }} ?>
								  <?  if($tur != 'Project'){
									  $ceksi = mysql_query("select * from kategori where tur = '$katGrubu' and id != '$thisKat'  order by sira asc");
										while($yazsi = mysql_fetch_array($ceksi)){
								  ?>
                                  <option value="<?=$yazsi["id"]?>"><?=$yazsi["ad"]?></option>
								  <? }} ?>
								  </select>
							  </div>
							</div>

							 <?} ?>
							 
							 <? if($tur == 'Referans'){?>
							<div class="section">
                              <label>Kategori<small>Kategori Seçiniz</small></label>
                              <div>
                              	  <select data-placeholder="Seçiniz" name="kategori" class="chzn-select" tabindex="2" >

								  <? $ceksi = mysql_query("select * from kategori where tur = 'Odul_category' and id = '".$cek["kategori"]."'  order by sira asc");
										while($yazsi = mysql_fetch_array($ceksi)){
								  ?>
                                  <option value="<?=$yazsi["id"]?>"><?=$yazsi["ad"]?></option>
								  <? } ?>
								  <? $ceksi = mysql_query("select * from kategori where tur = 'Odul_category' and id != '".$cek["kategori"]."'  order by sira asc");
										while($yazsi = mysql_fetch_array($ceksi)){
								  ?>
                                  <option value="<?=$yazsi["id"]?>"><?=$yazsi["ad"]?></option>
								  <? } ?>
								  </select>
							  </div>
							</div>

						
                              <div class="section">
                                  <label> Ödül Kategorisi  <small></small></label>
                                  <div> <input type="text" name="ajans"  value="<?=$cek["ajans"];?>" class=" medium" /><span class="f_help"></span></div>
                             </div>
                             <div class="section">
                                  <label> Ödül URL  <small></small></label>
                                  <div> <input type="text" name="kreatif"  value="<?=$cek["kreatif"];?>" class=" medium" /><span class="f_help"></span></div>
                             </div>
							 <? } ?>
                             <? if($tur!= 'Menu' && $tur != 'StaticPage' && $tur != 'Home'){?>
							<? if($cek["resim"] != ''){$image = '../statics/kare/'.$cek["resim"];}else{$image = 'images/no.jpg';} ?>
                            <div class="section ">
                            <label>Var Olan Resim <small></small></label>
                            <div>
                                <img src="<?=$image ?>" style="border:1px solid #777;">
                            </div>
                            </div>


							<div class="section ">
								<label> Resim <small>Resim Yükle</small></label>
								<div>
									<input type="file" name="resim" class="fileupload" />
                                    <span class="f_help">
                                    <?php 
                                        if($tur == 'Slider'){echo "1500px * 700px jpg";} 
                                        if($tur == 'Page'){echo "800px * 520px jpg";} 
                                        if($tur == 'Client'){echo "212px * 46px jpg";} 
                                    ?>
                                    </span>
								</div>
                            </div>
                            
                             <?php } ?>

                             <? if( $tur == 'Page' ){?>
							<div class="section">
								<label>Sağ  Açıklama<small>Kısa Açıklaması</small></label>
								<div>
								<textarea cols="60" rows="10" name="yer" id="location3" class="ckeditor"><?=$cek["yer"]?></textarea>
								<script language="javascript" type="text/javascript">
                                CKEDITOR.replace('yer',{
                                    filebrowserBrowseUrl: '/doris/browser/browse.php',
                                    filebrowserImageBrowseUrl: '/doris/browser/browse.php?type=Images',
                                    filebrowserUploadUrl: '/doris/uploader/upload.php',
                                    filebrowserImageUploadUrl: '/doris/uploader/upload.php?type=Images',
                                    filebrowserWindowWidth: '900',
                                    filebrowserWindowHeight: '400',
                                    filebrowserBrowseUrl: 'ckfinder/ckfinder.html',
                                    filebrowserImageBrowseUrl: 'ckfinder/ckfinder.html?Type=Images',
                                    filebrowserFlashBrowseUrl: 'ckfinder/ckfinder.html?Type=Flash',
                                    filebrowserUploadUrl: 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                                    filebrowserImageUploadUrl: 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                                    filebrowserFlashUploadUrl: 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
                                });
                            </script>
								<span class="f_help">Metinleri yapıştırmadan önce not defterine yapıştırıp daha sonra buraya yapıştırınız.</span></div>
                            </div>
							<? } ?>

							<? if($tur == 'Home' || $tur == 'Page'  || $tur == 'StaticPage' ){?>
							<div class="section">
								<label>İçerik  Açıklama<small>Detaylı Açıklaması</small></label>
								<div>
								<textarea cols="60" rows="10" name="aciklama" id="location2" class="ckeditor"><?=$cek["aciklama"]?></textarea>
								<script language="javascript" type="text/javascript">
                                CKEDITOR.replace('aciklama',{
                                    filebrowserBrowseUrl: '/doris/browser/browse.php',
                                    filebrowserImageBrowseUrl: '/doris/browser/browse.php?type=Images',
                                    filebrowserUploadUrl: '/doris/uploader/upload.php',
                                    filebrowserImageUploadUrl: '/doris/uploader/upload.php?type=Images',
                                    filebrowserWindowWidth: '900',
                                    filebrowserWindowHeight: '400',
                                    filebrowserBrowseUrl: 'ckfinder/ckfinder.html',
                                    filebrowserImageBrowseUrl: 'ckfinder/ckfinder.html?Type=Images',
                                    filebrowserFlashBrowseUrl: 'ckfinder/ckfinder.html?Type=Flash',
                                    filebrowserUploadUrl: 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
                                    filebrowserImageUploadUrl: 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
                                    filebrowserFlashUploadUrl: 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash'
                                });
                            </script>
								<span class="f_help">Metinleri yapıştırmadan önce not defterine yapıştırıp daha sonra buraya yapıştırınız.</span></div>
                            </div>
							<? } ?>
							<div class="section">
                                  <label> Alt Açıklama  <small></small></label>
                                  <div> <textarea  name="ozet"   class=" large" ><?=$cek["ozet"]?></textarea><span class="f_help"></span></div>
                             </div>

							   <div class="section">
                                  <label> Etiketler <small>SEO (Keyword)</small></label>
                                  <div> <input type="text" name="etiket"  value="<?=$cek["etiket"]?>" class=" large" /><span class="f_help">Yazı İçindeki Anahtar Kelimeler</span></div>
                             </div>

                              <div class="section last">
                                  <div>

								 <input type="submit" value="Kaydet" class="uibutton loading" title="Kaydet" rel="1" />
								  </div>

							 </div>
                          </form>
                        </div>
                        </div>

					   <?php include("fouter.php") ?>
					</div>
            </div>
</body>
</html>

<?php }

Function ekle(){
global $uye;
$uploadtype = 'edit';

include('upload_set.php');

}
$Git = $_GET["Git"];
switch($Git){

	default:
	baslangic();
	break;

	 case "ekle":
     ekle();
	 break;

}?>
