<?
include("inc/config.php");
include("inc/fonk.php");
$ceks = mysql_fetch_array(mysql_query("select * from ayar where dil = 'tr'"));
$link = 'uye-ol-3';
$cek = mysql_fetch_array(mysql_query("select * from kategori where link = '$link' and tur = 'StaticPage' and durum = '1' and dil = 'tr'"));


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?=$cek["title"]?></title>
	<meta name="robots" content="index, follow" >
    <meta name="description" content="<?=$cek["description"]?>" />    
    <meta name="author" content="">
    <!--[if lt IE 9]>
	<script src="assets/js/html5shiv.js"></script>
	<![endif]-->


    <!-- CSS Files
    ================================================== -->
    <link rel="stylesheet" href="assets/css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="assets/css/jpreloader.css" type="text/css">
    <link rel="stylesheet" href="assets/css/animate.css" type="text/css">
    <link rel="stylesheet" href="assets/css/plugin.css" type="text/css">
    <link rel="stylesheet" href="assets/css/owl.carousel.css" type="text/css">
    <link rel="stylesheet" href="assets/css/owl.theme.css" type="text/css">
    <link rel="stylesheet" href="assets/css/owl.transitions.css" type="text/css">
    <link rel="stylesheet" href="assets/css/magnific-popup.css" type="text/css">
    <link rel="stylesheet" href="assets/css/style.css" type="text/css">
    <link rel="stylesheet" href="assets/demo/demo.css" type="text/css">

    <!-- custom background -->
    <link rel="stylesheet" href="assets/css/bg.css" type="text/css">

    <!-- color scheme -->
    <link rel="stylesheet" href="assets/css/color.css" type="text/css" id="colors">

    <!-- load fonts -->
    <link rel="stylesheet" href="assets/fonts/font-awesome/css/font-awesome.css" type="text/css">
    <link rel="stylesheet" href="assets/fonts/fontawesome/css/all.css" type="text/css">
    <link rel="stylesheet" href="assets/fonts/elegant_font/HTML_CSS/style.css" type="text/css">
    <link rel="stylesheet" href="assets/fonts/et-line-font/style.css" type="text/css">

    <!-- revolution slider -->
    <link rel="stylesheet" href="rs-plugin/css/settings.css" type="text/css">
    <link rel="stylesheet" href="assets/css/rev-settings.css" type="text/css">
</head>
<body class="page-about">

    <div id="wrapper">

    <?php include('header.php'); ?>
    
            <!-- content begin -->
            <div id="content">
               
    
                <div class="container">
                    <div class="row">
    
                        <div class="col-md-12">
                            <h2>Turkish Click'e Katıl</h2>
                            <form class="">
                                <div class="row form-content">
                                    <h4 class="col-md-12">Kişisel Bilgileriniz</h4>
                                    <div class="form-group col-md-6">
                                        <label>Adınız Soyadınız(*)</label>
                                        <input type="text" class="form-control" required >
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Firma Adınız (*)</label>
                                        <input type="text" class="form-control" required >
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Email (*)</label>
                                        <input type="email" class="form-control" required >
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Telefon (*)</label>
                                        <input type="tel" class="form-control" required >
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Mesajınız (*)</label>
                                        <textarea class="form-control" required ></textarea>
                                    </div>
                                    

                                </div>

                           

                                <div class="row form-content social-content">
                                    <div class="form-group form-check col-md-12">
                                        <input type="checkbox" class="form-check-input" id="sozlesme">
                                        <label class="form-check-label autos" for="sozlesme"><a href="#">Sözleşmeyi</a> Kabul Ediyorum.</label>
                                    </div>
                                    <div class="form-group form-check col-md-6">
                                        <input style="display: inline-block;" type="checkbox" class="form-check-input" id="gizlilik">
                                        <label style="display: contents;" class="form-check-label autos" for="gizlilik">Başvurumda yer alan kişisel verilerimin işleme alınması, üçüncü şahıslarla paylaşılması, değerlendirme, sahtekarlık önleme ve diğer yasa dışı faaliyetler için rıza gösteriyorum.</label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">TEKLİF İSTE</button>
                                </div>
                                
                        </form>
                        </div>
    
               
                    </div>
                </div>
            </div>

            <?php include('footer.php'); ?>
    </div>
    <!-- style switcher
    ================================================== -->
    


    <!-- Javascript Files
    ================================================== -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/jpreLoader.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/jquery.isotope.min.js"></script>
    <script src="assets/js/easing.js"></script>
    <script src="assets/js/jquery.flexslider-min.js"></script>
    <script src="assets/js/jquery.scrollto.js"></script>
    <script src="assets/js/owl.carousel.js"></script>
    <script src="assets/js/jquery.countTo.js"></script>
    <script src="assets/js/classie.js"></script>
    <script src="assets/js/video.resize.js"></script>
    <script src="assets/js/validation.js"></script>
    <script src="assets/js/wow.min.js"></script>
    <script src="assets/js/jquery.magnific-popup.min.js"></script>
    <script src="assets/js/enquire.min.js"></script>
    <script src="assets/js/designesia.js"></script>
    <script src="assets/demo/demo.js"></script>

</body>
</html>
